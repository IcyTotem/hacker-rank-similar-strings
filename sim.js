'use strict';

const MAX_HASH_SIZE = 13;

// This class checks similarity between substrings
class Palantir {
    // universe is the big global string given by the problem
    constructor (universe) {
        this.universe = Palantir.anonymize(universe);

        // This is an helping variable used to anonymize the temporary
        // substrings analyzed in scan(). It's useful to have only one
        // to avoid reinitializing useless arrays over and over
        this.referenceDictionary = new Array(10);
        this.windowDictionary = new Array(10);

        this.prepareHashes();
    }

    // An inverse map from 13-characters hashes to an index in the string makes it easier to find possible matching positions
    prepareHashes () {
        this.hashPositions = new Map();

        for (let i = 0; i < this.universe.length - MAX_HASH_SIZE; ++i) {
            const h = this.hash(i);
            let bucket = this.hashPositions.get(h);

            if (!bucket)
                this.hashPositions.set(h, bucket = []);

            bucket.push(i);
        }
    }

    // Transform a string into a number array
    static anonymize (a) {
        const result = new Array(a.length);
        const dictionary = new Array(10);
        dictionary.fill(0);

        let counter = 1;
        for (let i = 0; i < a.length; ++i) {
            const index = a.charCodeAt(i) - 97;
            result[i] = dictionary[index] || (dictionary[index] = counter++);
        }

        return result;
    }

    hash (leftBound) {
        const max = Math.min(this.universe.length - leftBound, MAX_HASH_SIZE);
        const dictionary = new Array(10);
        let result = 1319;
        let counter = 1;

        dictionary.fill(0);

        for (let i = 0; i < max; ++i) {
            const index = this.universe[leftBound + i] - 1;
            const placeholder = dictionary[index] || (dictionary[index] = counter++);
            result = placeholder * result + 17;
        }

        return result;
    }

    /*
     * Check whether the anonymized version of the substring of universe
     * starting at windowLeftBound is equal to anonymizedReference.
     * @return
     *   - -1: if the two are different in length;
     *   - k in [0, L): if there is a difference at position k;
     *   - L: if the two are equal (hence the superseded strings are similar).
     */
    scan (referenceLeftBound, windowLeftBound, length) {
        if (this.universe.length < windowLeftBound + length)
            return -1;

        this.referenceDictionary.fill(0);
        this.windowDictionary.fill(0);

        for (let i = 0; i < length; ++i) {
            const ri = this.universe[referenceLeftBound + i];
            const wi = this.universe[windowLeftBound + i];
            const rp = this.referenceDictionary[ri - 1];
            const wp = this.windowDictionary[wi - 1];

            if ((rp === 0) && (wp === 0)) {
                this.referenceDictionary[ri - 1] = wi;
                this.windowDictionary[wi - 1] = ri;
            } else if ((wp !== ri) || (rp !== wi)) {
                return i;
            }
        }

        return length;
    }

    similarSubstrings (l, r) {
        const length = r - l + 1;

        // The whole universe is the only element similar to itself
        if (length === this.universe.length)
            return 1;

        // The single character is similar to any other character
        if (length === 1)
            return this.universe.length;

        let matches = 0;

        if (length > MAX_HASH_SIZE) {
            const h = this.hash(l);
            const positions = this.hashPositions.get(h);

            for (let i of positions) {
                if (i === l) {
                    matches++;
                } else {
                    const score = this.scan(l, i, length);
                    if (score === length)
                        matches++;
                }
            }
        }

        // Hash collision? String too short?
        if (matches === 0) {
            // Standard scenario: scan all possible indices in the universe
            // to find potential similar substrings
            const upperBound = this.universe.length - length + 1;

            for (let i = 0; i < upperBound; ++i) {
                if (i === l) {
                    matches++;
                } else {
                    const score = this.scan(l, i, length);
                    if (score === length)
                        matches++;
                }
            }
        }

        return matches;
    }
}

const microtime = () => {
    const hrTime = process.hrtime();
    return hrTime[0] * 1000000 + hrTime[1] / 1000;
};

const processInput = (input) => {
    const lines = input.split('\n');
    const mainParams = lines[0].split(' ').map(Number);

    const n = mainParams[0];
    const q = mainParams[1];
    const string = lines[1].trim();
    const palantir = new Palantir(string);
    let totalTime = 0;

    for (let i = 0; i < q; ++i) {
        const queryParams = lines[2 + i].split(' ').map(Number);
        const l = queryParams[0] - 1;
        const r = queryParams[1] - 1;
        const result = palantir.similarSubstrings(l, r);
        console.log(result);
    }
};

let input = '';

process.stdin.resume();
process.stdin.setEncoding('ascii');
process.stdin.on('data', data => input += data);
process.stdin.on('end', () => processInput(input));
